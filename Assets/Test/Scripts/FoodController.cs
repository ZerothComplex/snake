﻿using System.Collections;
using UnityEngine;

public class FoodController : MonoBehaviour {

    [SerializeField]
    private GameObject foodPrefab;
    [SerializeField]
    private Transform environment;
    [SerializeField]
    private Collider ground;

    public static int count = 0;

    private void Start()
    {
        StartCoroutine(SpawnFoodCo());
    }

    private IEnumerator SpawnFoodCo()
    {
        while(true)
        {
            if (count < 3) //Only 3 food at a time
            {
                if (count != 0)
                    yield return new WaitForSeconds(3f); //3 sec wait in between food spawning
                SpawnFood();
                
            }
            yield return null;
        }
    }


    [ContextMenu("Spawn")]
    public void SpawnFood()
    {
        var posX = Random.Range(-4f, 4f);
        var posZ = Random.Range(-4f, 4f);

        var pos = new Vector3(posX, 0f, posZ);

        //Check if not food random position is inside the bounds of ground
        if (!ground.bounds.Contains(pos))
        {
            //Spawn again discarding the previous random position
            SpawnFood();
            return;
        }

        var cube = Instantiate(foodPrefab, pos, Quaternion.identity);
        cube.transform.SetParent(environment);
        count++;
    }
}
