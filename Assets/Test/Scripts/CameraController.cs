﻿using System.Collections;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform forwardCameraPos;
    public Transform backwardCameraPos;

    public Transform camPosParent;

    private PlayerMovement playerMovement;
    private Transform playerTransform;
    private Vector3 offset;
    private float posY;

    private void Start()
    {
        playerMovement = PlayerMovement.Instance;
        playerTransform = playerMovement.transform;
        posY = playerTransform.position.y;
        offset = transform.position - playerTransform.position;
    }

    private void OnEnable()
    {
        PlayerMovement.Instance.OnCameraSwitch += CameraSwitch;
    }

    private void LateUpdate()
    {
        camPosParent.rotation = playerTransform.rotation;

        if (!playerMovement.isSwitchingCamera)
            transform.position = playerTransform.position + offset;
    }

    public void CameraSwitch()
    {
        if (playerMovement.isSwitchingCamera)
            return;

        playerMovement.isSwitchingCamera = true;

        Vector3 endPos;

        if (!playerMovement.isForwardPos)
        {
            endPos = forwardCameraPos.position;
            playerMovement.isForwardPos = true;
        }
        else
        {
            endPos = backwardCameraPos.position;
            playerMovement.isForwardPos = false;
        }

        StartCoroutine(SwitchView(transform.position, endPos, 1f));
    }

    IEnumerator SwitchView(Vector3 startPos, Vector3 endPos, float time)
    {
        var elapsedTime = 0f;
        while (elapsedTime < time)
        {
            transform.position = Vector3.Lerp(startPos, endPos, (elapsedTime / time));
            transform.LookAt(playerTransform);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.LookAt(playerTransform);
        offset = transform.position - playerTransform.position;
        playerMovement.isSwitchingCamera = false;
    }

}
