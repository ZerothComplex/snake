﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class SliderController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    [SerializeField]
    private Transform slider;

    Coroutine rotateSliderCo;
    Coroutine setToDefaultCo;
    PlayerMovement playerMovement;

    private void Start()
    {
        playerMovement = PlayerMovement.Instance;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (rotateSliderCo != null)
            StopCoroutine(rotateSliderCo);

        if (setToDefaultCo != null)
            StopCoroutine(setToDefaultCo);

        rotateSliderCo = StartCoroutine(RotateSlider());
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (rotateSliderCo != null)
            StopCoroutine(rotateSliderCo);

        if (setToDefaultCo != null)
            StopCoroutine(setToDefaultCo);

        //Set default rotation and turn direction
        StartCoroutine(SetToDefault(slider.rotation, Quaternion.identity, 0.5f));
        playerMovement.SnakeTurnDirection = PlayerMovement.TurnDirection.Default;
    }

    IEnumerator SetToDefault(Quaternion startRot, Quaternion endRot, float time)
    {
        var elapsedTime = 0f;
        while (elapsedTime < time)
        {
            slider.rotation = Quaternion.Lerp(startRot, endRot, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator RotateSlider()
    {
        float previousAngle = 0f;

        while (true)
        {
            Vector3 mousePos = Input.mousePosition;

            //Get rotation towards mouse position
            var rot = Quaternion.LookRotation(Vector3.forward, mousePos - slider.position);
            var angle = rot.eulerAngles.z;

            //Check if slider is turned left or right
            if (angle > previousAngle)
            {
                playerMovement.SnakeTurnDirection = PlayerMovement.TurnDirection.Left;
            }
            else if(angle < previousAngle)
                playerMovement.SnakeTurnDirection = PlayerMovement.TurnDirection.Right;

            previousAngle = angle;

            slider.rotation = rot;
            playerMovement.Turn(angle);

            yield return null;
        }
    }

    
}
