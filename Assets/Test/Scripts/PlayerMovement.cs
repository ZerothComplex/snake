﻿using System.Collections;
using UnityEngine;
using System;

public class PlayerMovement : MonoBehaviour
{
    private static PlayerMovement instance;
    public static PlayerMovement Instance {

        get {

            if (instance == null)
                instance = FindObjectOfType<PlayerMovement>();

            return instance;
        }
    }

    public enum TurnDirection
    {
        Default = 0,
        Right  = 1,
        Left = 2
    }

    [System.Serializable]
    public class Player
    {
        public GameObject gameobject;
        public Rigidbody rigidbody;
        public Animation animation;
        public Vector3 direction = Vector3.forward;
        public float speed = 1f;
        public TurnDirection turnDirection;
       
        public bool isForward;
    }

    public TurnDirection SnakeTurnDirection { get { return player.turnDirection; } set { player.turnDirection = value; } }

    [SerializeField]
    private Player player;
    [SerializeField]
    private string boundTag = "Bounds";
    [SerializeField]
    private string foodTag = "Food";

    public bool isForwardPos = false;
    public bool isSwitchingCamera = false;

    public Action OnCameraSwitch;

    private float prevPosY;

    private void Start()
    {
        prevPosY = player.gameobject.transform.rotation.eulerAngles.y;
    }

    private void FixedUpdate()
    {
        Move();
    }

    public void Move()
    {
        player.rigidbody.velocity = (player.direction * player.speed);
        transform.position = new Vector3(transform.position.x, 0f, transform.position.z);
    }

    /// <summary>
    /// Inverse the rotation when object collides with bounds
    /// </summary>
    public void AboutTurn()
    {
        player.gameobject.transform.Rotate(Vector3.up * 180);
        player.direction *= -1;
    }

    public void Turn(float angle)
    {
        
        angle = 360 - angle;
        angle = angle / 2;

        if (!isSwitchingCamera)
        {
            if (angle > 45)
            {
                if (OnCameraSwitch != null)
                    OnCameraSwitch();

            }
        }


        if (player.turnDirection == TurnDirection.Left)
        {
            var _angle = angle;
            _angle -= 180; //So that left turn value would increase gradualy instead of taking first value as 180
            StartCoroutine(TurnCo(player.gameobject.transform.rotation, _angle));

        }
        else if (player.turnDirection == TurnDirection.Right)
        {
            angle = Mathf.Abs(angle);
            StartCoroutine(TurnCo(player.gameobject.transform.rotation, angle));
        }

    }

    private IEnumerator TurnCo(Quaternion startingRot, float angle, float time = 0.35f)
    {
        float elapsedTime = 0;

        var newRot = Quaternion.Euler(0, startingRot.eulerAngles.y + angle, 0);

        while (elapsedTime < time)
        {
            if (elapsedTime >= 1)
                elapsedTime = 1;

            player.gameobject.transform.rotation = Quaternion.Lerp(startingRot, newRot, (elapsedTime / time));
            elapsedTime += Time.deltaTime;

            yield return null;
        }

        player.direction = player.gameobject.transform.forward;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(boundTag))
        {
            AboutTurn();
        }

        if (collision.gameObject.CompareTag(foodTag))
        {
            Destroy(collision.gameObject);
            FoodController.count--;
        }

    }

}
